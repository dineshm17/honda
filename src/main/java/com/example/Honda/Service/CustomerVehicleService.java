package com.example.Honda.Service;

import com.example.Honda.entity.Customer;
import com.example.Honda.entity.Vehicle;
import com.example.Honda.repository.CustomerRepository;
import com.example.Honda.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;
import java.util.Optional;

@Service
public class CustomerVehicleService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private VehicleRepository vehicleRepository;

    public Customer saveCustomerWithVehicle(Customer customer)
    {
        return  customerRepository.save(customer);
    }


}
