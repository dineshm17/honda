package com.example.Honda.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="CustomerTableFinal")
public class Customer {

    @Id
    @GeneratedValue
    private long cid;
    private String name;

    private int age;
    private String gender;

    @ManyToMany(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(name="CustomerVehicleRelationship",
   joinColumns = {
            @JoinColumn(name="customer_id",referencedColumnName = "cid")},
            inverseJoinColumns = {
            @JoinColumn(name="vehicle_id",referencedColumnName = "vid")}

    )

    private Set<Vehicle> vehicles;

    public long getCid() {
        return cid;
    }

    public void setCid(long cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Customer(long cid, String name, int age, String gender, Set<Vehicle> vehicles) {
        this.cid = cid;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.vehicles = vehicles;
    }

    public Customer(long cid, String name, int age, String gender) {
        this.cid = cid;
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public Customer() {

    }
}

