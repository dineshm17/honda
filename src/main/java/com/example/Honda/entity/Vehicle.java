package com.example.Honda.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="VehicleTableFinal")
public class Vehicle {

    @Id
    @GeneratedValue
    private long vid;
    private String vehiclename;
    private int qty;
    private int price;

    @ManyToMany(mappedBy = "vehicles",fetch = FetchType.LAZY)
    private Set<Customer> customers;

    public long getVid() {
        return vid;
    }

    public void setVid(long vid) {
        this.vid = vid;
    }

    public String getVehiclename() {
        return vehiclename;
    }

    public void setVehiclename(String vehiclename) {
        this.vehiclename = vehiclename;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Vehicle(long vid, String vehiclename, int qty, int price, Set<Customer> customers) {
        this.vid = vid;
        this.vehiclename = vehiclename;
        this.qty = qty;
        this.price = price;
        this.customers = customers;
    }

    public Vehicle(long vid, String vehiclename, int qty, int price) {
        this.vid = vid;
        this.vehiclename = vehiclename;
        this.qty = qty;
        this.price = price;
    }



    public Vehicle() {
    }
}
