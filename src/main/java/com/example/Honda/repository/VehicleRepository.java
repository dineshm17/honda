package com.example.Honda.repository;

import com.example.Honda.entity.Customer;
import com.example.Honda.entity.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle,Long> {

    Optional<Vehicle> findVehicleByVehiclename(String vehiclename);

}
