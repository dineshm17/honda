package com.example.Honda.controller;

import com.example.Honda.Service.CustomerVehicleService;
import com.example.Honda.entity.Customer;
import com.example.Honda.repository.CustomerRepository;
import com.example.Honda.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/vehicle")
public class CustomerVehicleController {


    @Autowired
    private CustomerVehicleService customerVehicleService;

    @PostMapping
    public Customer saveCustomerWithVehicle(@RequestBody Customer customer)
    {
        return customerVehicleService.saveCustomerWithVehicle(customer);
    }



}
