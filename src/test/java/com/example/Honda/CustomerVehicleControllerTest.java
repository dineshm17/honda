package com.example.Honda;

import com.example.Honda.controller.CustomerVehicleController;
import com.example.Honda.entity.Customer;
import com.example.Honda.entity.Vehicle;
import com.example.Honda.repository.CustomerRepository;
import com.example.Honda.repository.VehicleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CustomerVehicleControllerTest {

    @Autowired
    private CustomerVehicleController customerVehicleController;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private VehicleRepository vehicleRepository;


    @Test
    void checkWhetherDataInserted()
    {
        Vehicle veh1 =new Vehicle(78L,"yamaha",3,100);

        Set<Vehicle> vehicles =new HashSet<>();

        vehicles.add(veh1);

        Customer customer = new Customer(11L,"felix",22,"male",vehicles);

        customerVehicleController.saveCustomerWithVehicle(customer);

        assertNotNull(customerRepository.findCustomerByName("felix").get(),"customer data not inserted properly");

        assertNotNull(vehicleRepository.findVehicleByVehiclename("yamaha").get(),"Vehicle data not inserted properly");



    }



}